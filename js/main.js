// const { active } = require("browser-sync");

// header
var HeaderCtrl = (function () {
  var header, gnb, body;

  var toggle = function (target) {
    if(!target.hasClass('actived')) {
      if($('.museum').length > 0){
        $('.parallax02__leftinner').addClass('zindex3')
      }
      if($('.sub__nav').length > 0){
        $('.sub__nav').css('z-index','3')
      }
      setTimeout(function() {
        open(target);
      }, 100);
    } else {
      close(target);
      if($('.sub__nav').length > 0){
        $('.sub__nav').removeAttr('style')
      }
      setTimeout(function() {
        if($('.museum').length > 0){
          $('.parallax02__leftinner').removeClass('zindex3')
        }
      }, 500);
    }
  }

  var open = function(target) {
      target.addClass('actived');
      header.self.addClass('sticky-actived');
      header.self.find(gnb.self).addClass('actived');
      body.self.addClass('hidden')
  }

  var close = function(target) {
    target.removeClass('actived');
    header.self.removeClass('sticky-actived');
    header.self.find(gnb.self).removeClass('actived');
    body.self.removeClass('hidden')
  }

  var event = function() {
    header.toggle.on('click', function() {
      var target = $(this);
      toggle(target);
    });

    gnb.item.on({
      mouseover: function() {
        $(this).addClass('actived');
        $(this).find(gnb.subList).addClass('actived');
        $(this).siblings().removeClass('actived').find(gnb.subList).removeClass('actived');
      },
      // mouseout: function() {
      //   $(this).removeClass('actived');
      //   $(this).find(gnb.subList).removeClass('actived');
      // },
      click: function(){
        return false
      }
    });

    gnb.lang.on('click', function(e){
      var $this = $(this);
      if(!$this.is('.actived')){
        gnb.lang.removeClass('actived')
        $this.addClass('actived');
      }
    })

    gnb.box.append('<div class="gnb__area"></div>')
    $('.gnb__area').on('mouseenter', function(){
      gnb.item.removeClass('actived').find(gnb.subList).removeClass('actived');
    })
  }

  var init = function () {
    header = {
      self: $('.header'),
      toggle: $('.header__hamburger')
    };

    gnb = {
      self: $('.gnb'),
      item: $('.gnb__item'),
      subList: $('.gnb__sublist'),
      lang: $('.gnb__translatorbutton'),
      box: $('.gnb__menu')
    }

    body = {
      self: $('body')
    }

    event();
  };
  return {
    init: init
  };
})();

// mobile header
var MobileHeaderCtrl = (function () {
  var init = function () {
    $('.gnb__list > li:has(ul)').addClass("has-sub");
    $('.gnb__list > li > a').click(function() {

      var checkElement = $(this).next();
      $('.gnb__list li').removeClass('active');
      $(this).closest('li').addClass('active');

      if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
        $(this).closest('li').removeClass('active');
        checkElement.slideUp('normal');
      }
      if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
        $('.gnb__list li ul:visible').slideUp('normal');
        checkElement.slideDown('normal');
      }
      if (checkElement.is('ul')) {
        return false;
      } else {
        return true;
      }
    });
  };
  return {
    init: init
  };
})();

// swiper
var SwiperCtrl = (function () {
  // main : top
  var swiperMain = function() {
    var video = $(".video__item video");
    var swiper = new Swiper('.video__carousel', {
      effect: 'fade',
      autoplay : {
        delay : 5000,
        disableOnInteraction : false,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      }
    });
    swiper.on('slideChange', function () {
      if(swiper.activeIndex === 0){
        video[0].currentTime = 0;
      }
    });
  }
  var swiperMainMob = function() {
    var video = $(".video__item video");
    var swiper = new Swiper('.main-swiper', {
      effect: 'fade',
      autoplay : {
        delay : 5000,
        disableOnInteraction : false,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      }
    });
    swiper.on('slideChange', function () {
      if(swiper.activeIndex === 0){
        video[0].currentTime = 0;
      }
    });
  }
  // main : directiors
  var swiperDirectiors = function(){
    var swiper = new Swiper('.directors__list', {
      loop: true,
      touchRatio: 0,
      autoheight:'true',
      effect: 'fade',
      pagination: {
          el: '.directors__pagination',
          clickable: true,
          renderBullet: function (index, className) {
              return '<span class="' + className + '">' + "0" + (index + 1) + '</span>';
          },
      },
    });
  }

  // gallerythumbs
  var swiperGalleryThumbs = function() {
    var galleryThumbs = new Swiper('.gallery-thumbs', {
      spaceBetween: 0,
      slidesPerView: 1,
      loop: true,
      touchRatio: 0,
      freeMode: true,
      loopedSlides: 1, //looped slides should be the same
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
    });
    var galleryTop = new Swiper('.gallery-top', {
      spaceBetween: 0,
      loop: true,
      touchRatio: 0,
      loopedSlides: 5, //looped slides should be the same
      autoplay : {
        delay : 5000,
        disableOnInteraction : false,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      thumbs: {
        swiper: galleryThumbs,
      },
    });

    // a61 swiper
    var swiper = new Swiper('.img__carousel', {
      autoplay: {
        delay: 5500,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
          return '<span class="' + className + '">' + (index + 1) + '</span>';
        },
      },
    });
  }

  // main : semiconductor
  var swiperSemiconductor = function() {
    slide = {
      self: '.semiconductor__slide',
      item: '.semiconductor__item',
      bullet: '.swiper-pagination-bullet',
      bulletActive: '.swiper-pagination-bullet-active',
      progress: '.semiconductor__timerbar'
    }

    box = {
      list: '.semiconductor-box__list',
      item: '.semiconductor-box__item',
      text: '.semiconductor-box__text',
      image: '.semiconductor-box__background img'
    }

    swiper = new Swiper(slide.self, {
      allowTouchMove: false,
      autoplay : {
        delay : 3000,
        disableOnInteraction : false,
      },
      pagination: {
        el: '.semiconductor__pagination',
        clickable: true,
        renderBullet: function (index, className) {
          return '<button type="button" class="' + className + '">' + $(slide.item).eq(index).attr('data-name') +
          '<span class="semiconductor__timer"> <span class="semiconductor__timerbar"> </span>' + '</button>';
        }
      }
    });

    var slideItem = $(box.list).find(box.item);

    $(slide.bulletActive).find(slide.progress).animate({width: '100%'});
    slideItem.eq(0).css({'z-index' : '5'}).siblings().css({'z-index' : '1'});
    slideItem.eq(0).find(box.image).css({'transform' : 'scale(1.1)'}).parents(box.item).siblings().find(box.image).css({'transform' : 'scale(2.0)', 'bottom' : '-200px'});
    slideItem.eq(0).find(box.text).css({'opacity' : '1', 'bottom' : '0'}).parents(box.item).siblings().find(box.text).css({'opacity' : '0', 'bottom' : '-30px'});

    swiper.on('slideChange', function() {
      var activeIndex = swiper.activeIndex;
      slideItem.eq(activeIndex).css({'z-index' : '5'}).siblings().css({'z-index' : '1'});
      slideItem.eq(activeIndex).find(box.image).css({'transform' : 'scale(1.1)', 'bottom' : '0'}).parents(box.item).siblings().find(box.image).css({'transform' : 'scale(2.0)', 'bottom' : '-200px'});
      slideItem.eq(activeIndex).find(box.text).css({'opacity' : '1', 'bottom' : '0'}).parents(box.item).siblings().find(box.text).css({'opacity' : '0', 'bottom' : '-30px'});
      $(slide.bulletActive).find(slide.progress).animate({width: '100%'});
      $(slide.bullet).find(slide.progress).css({width: 0});
    });

  }

  // e51
  var swiperTriple = function(){
    var swiper = new Swiper('.slider__triple--container', {
      slidesPerView: 3,
      spaceBetween: 15,
      slidesPerGroup: 1,
      loop: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  }

  // c21
  var arr_dot_slider = function(){
    var swiper = new Swiper('.arr__dot--carousel', {
      slidesPerView: 1,
      slidesPerGroup: 1,
    //  effect: 'fade',
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  }


  // b30
  var processSlide = function() {
    var swiper = new Swiper('.process__slide', {
      pagination: {
        el: '.process__pagination',
        clickable: true,
        renderBullet: function (index, className) {
            return '<button type="button" class="' + className + '">' + '<span class="process__bulletindex">' + 0 + (index + 1) + '</span>' + '<span class="process__bullettext">' + $('.process__slideitem').eq(index).attr('data-process-name') + '</span>' + '</button>';
        },
      },
    });
  }

  var init = function () {
    swiperMain();
    swiperMainMob();
    swiperDirectiors();
    swiperGalleryThumbs();
    swiperSemiconductor();
    swiperTriple();
    arr_dot_slider();
    processSlide();
  };
  return {
    init: init
  };
})();

// input file
var InputFile = (function () {
  var init = function () {
    var fileTarget = $('.filebox .upload__name');

    fileTarget.on('change', function(){
        if(window.FileReader){
            var filename = $(this)[0].files[0].name;
        } else {
            var filename = $(this).val().split('/').pop().split('\\').pop();
        }
        $(this).siblings('.filebox__text').val(filename);
    });
  };
  return {
    init: init
  };
})();

// c11 slide
var NewsGalleryCtrl = (function () {
  var $obj;

  var prev = function () {
    var currentIndex = $obj.self.find('.actived').index();
    var panelLength = $obj.panel.length;

    $obj.panel.eq(currentIndex).removeClass('actived');
    $obj.anchor.eq(currentIndex).removeClass('actived');
    $obj.text.eq(currentIndex).removeClass('actived');

    currentIndex = currentIndex - 1;

    if(currentIndex < 0){
      $obj.panel.eq(panelLength - 1).addClass('actived');
      $obj.anchor.eq(panelLength - 1).addClass('actived');
      $obj.text.eq(panelLength - 1).addClass('actived');
    } else {
      $obj.panel.eq(currentIndex).addClass('actived');
      $obj.anchor.eq(currentIndex).addClass('actived');
      $obj.text.eq(currentIndex).addClass('actived');
    }
  }

  var next = function () {
    var currentIndex = $obj.self.find('.actived').index();
    var panelLength = $obj.panel.length;

    $obj.panel.eq(currentIndex).removeClass('actived');
    $obj.anchor.eq(currentIndex).removeClass('actived');
    $obj.text.eq(currentIndex).removeClass('actived');

    currentIndex = currentIndex + 1;

    if(currentIndex === panelLength){
      $obj.panel.eq(0).addClass('actived');
      $obj.anchor.eq(0).addClass('actived');
      $obj.text.eq(0).addClass('actived');
    } else {
      $obj.panel.eq(currentIndex).addClass('actived');
      $obj.anchor.eq(currentIndex).addClass('actived');
      $obj.text.eq(currentIndex).addClass('actived');
    }
  }

  var pagination = function (target, index) {
    if(!target.is('.actived')) {
      target.addClass('actived').siblings().removeClass('actived');
      $obj.panel.eq(index).addClass('actived').siblings().removeClass('actived');
      $obj.text.eq(index).addClass('actived').siblings().removeClass('actived');
    }
  }

  var slideImage = function (target, index) {
    var total = $obj.panel.length - 1;
    target.next().addClass('actived').siblings().removeClass('actived');
    $obj.anchor.eq(index + 1).addClass('actived').siblings().removeClass('actived');
    $obj.text.eq(index + 1).addClass('actived').siblings().removeClass('actived');

    if(index == total) {
      target.removeClass('actived');
      $obj.panel.eq(0).addClass('actived');
      $obj.anchor.removeClass('actived').eq(0).addClass('actived');
      $obj.text.removeClass('actived').eq(0).addClass('actived');
    }
  }

  var event = function () {

    $obj.anchor.on('click', function(e){
      e.preventDefault();
      var target = $(this);
      var index = target.index();
      pagination(target, index);
    });

    $obj.panel.on('click', function(){
      var target = $(this);
      var index = target.index();
      slideImage(target, index);
    });

    $obj.prev.on('click', function(e){
      e.preventDefault();
      prev();
    });

    $obj.next.on('click', function(e){
      e.preventDefault();
      next();
    });
  }

  var init = function () {
    $obj = {
      self: $('.news-gallery'),
      anchor: $('.news-gallery__anchor'),
      panel: $('.news-gallery__item'),
      text: $('.news-gallery__textitem'),
      prev: $('.news-gallery__prev'),
      next: $('.news-gallery__next')
    }
    event();
  }
  return {
    init: init
  }
})();

// video
var VideoCtrl = (function () {
  var video;

  var event = function () {
    video.controller.on('click', function() {
      $(this).closest(video.self).toggleClass('actived');
    })
    video.play.on('click', function(){
      $(this).siblings(video.item).get(0).play();
    })
    video.pause.on('click', function(){
      $(this).siblings(video.item).get(0).pause();
    })
  }
  var init = function () {
    video = {
      self: $('[data-role="video"]'),
      controller: $('[data-role="videoController"]'),
      play: $('[data-role="videoController"].play'),
      pause: $('[data-role="videoController"].pause'),
      item: $('[data-role="video"]').find('video')
    }
    event();
  };
  return {
    init: init
  };
})();

// mapview
var MapCtrl = (function () {
  var $obj;

  var action = function(target) {
    target.toggleClass('active');
    target.parents('.flex').siblings('.map__area').toggleClass('open')
  }

  var event = function() {
    $obj.this.on('click', function() {
      var target = $(this);
      action(target);
      if($(this).hasClass('active')){
        $(this).find('span').text('지도닫기');
      } else{
        $(this).find('span').text('지도보기');
      }
    })
  }

  var init = function () {
    $obj = {
      this: $('.map-view')
    }
    event();
  }
  return {
    init: init
  }
})();

// location
var Breadcrumbs = (function () {
  var $window, $obj;

  var toggle = function() {
    $obj.link.on('click', function(e){
      e.preventDefault();
      if($obj.depth.length < 1){
        return;
      }
      if($obj.target.is('.actived')) {
      //  $obj.link.hide(); // .sub__link
        $obj.link.addClass('closed');
        $obj.depth.addClass('actived');
      }
    });

    $obj.back.on('click', function(){
      $obj.link.removeClass('closed');
      $obj.depth.removeClass('actived');
    //  $obj.link.show();
    });
  }

  var action = function() {
    var currentOffset = $window.scrollTop();
    var clip = $('.clip');
    if(clip.length < 1){
      if(currentOffset < targetOffset) {
        $obj.target.removeClass('actived');
      }

      // if(currentOffset+300 > $obj.target.offset().top) {
      if(currentOffset >= $obj.target.offset().top) {
        if(!$obj.target.hasClass('actived')){
          $obj.target.addClass('actived');
        }
      }
    } else {
      $obj.target.addClass('actived');
    }

    if(!$obj.target.is('.actived')){
      $obj.link.show();
      $obj.depth.removeClass('actived');
    }
  }

  var event = function(){
    $window.on('scroll', function(){
      action();
    });
  }
  var targetOffset;
  var init = function(){
    $window = $(window);
    $obj = {
      target: $('.sub__location'),
      link: $('.sub__link'),
      depth: $('.sub__depthbox'),
      back: $('.sub__back')
    }
    if($obj.target.length < 1){
      return;
    } else {
      targetOffset= $obj.target.offset().top
    }
    event();
    action();
    toggle();
  }
  return {
    init: init
  }
})();

// top button
var TopbuttonCtrl = (function () {
  var $window, $obj;

  var displayDetectOffset = function() {
  //  if($window.scrollTop() > $('.sub').height() / 2) {
    if($window.scrollTop()) {
      $obj.this.fadeIn();
    } else {
      $obj.this.hide();
    }
  }

  var event = function(){
    $window.on('scroll', function(){
      var documentHeight = $(document).height();
      var footerHeight = $('.footer').innerHeight();
      var stopOffset = documentHeight - footerHeight - $obj.height - $obj.space;
      var restartOffset = documentHeight - footerHeight - $window.height()

      displayDetectOffset();

      if($obj.this.offset().top > stopOffset) {
        $obj.this.addClass('actived');
      }

      if($window.scrollTop() < restartOffset) {
        $obj.this.removeClass('actived');
      }
    });
    $obj.this.on('click', function(e){
      e.preventDefault();
      $('html, body, .popup__content, .popup').animate({scrollTop : 0}, 500);
      if($('.popup__header').hasClass('actived')){
        $('.popup__header').removeClass('actived')
      }
    })
  }
  var init = function(){
    if ($('.footer,.popup-footer').length < 1) {
      return;
    }

    $window = $(window);

    $obj = {
      this: $('.top-button'),
      height: $('.top-button').height(),
      space: 24
    }

    event();
    displayDetectOffset();
  }
  return {
    init: init
  }
})();

// b41-1
// var HorizontalInterection = (function () {
//   var link;
//   var swiperHorizontal = function() {
//     var swiper = new Swiper(".kvs__for", {
//       spaceBetween: 0,
//       slidesPerView: 1,
//       freeMode: true,
//       watchSlidesVisibility: true,
//       watchSlidesProgress: true,
//       touchRatio: 0,
//       autoHeight: true,
//       allowTouchMove:false
//     });
//     var swiper2 = new Swiper(".kvs__wrapper", {
//       slidesPerView: "auto",
//       centeredSlides: true,
//       spaceBetween: 0,
//       loop: true,
//       touchRatio: 0,
//       allowTouchMove:false,
//       navigation: {
//         nextEl: ".swiper-button-next",
//         prevEl: ".swiper-button-prev",
//       },
//       thumbs: {
//         swiper: swiper,
//       },
//     });

//   }


//   // var autoScroll = function() {
//   //   $('html, body').animate({scrollTop : 300}, 500)
//   // }

//   // var event = function() {
//   //   link.this.on('click', function(e){
//   //     e.preventDefault();
//   //     autoScroll();
//   //   })
//   // }

//   var init = function(){
//     // link = {
//     //   this: $('.sub__depthlink')
//     // }
//     swiperHorizontal();
//     // event();
//     // link.this.trigger('click');
//   }
//   return {
//     init: init
//   }
// })();

// scroll
var scrollBg = (function () {
  var init = function () {
    $(window).scroll(function(){
      $('.sub__location').find('.sub__link').removeClass('closed');
      $('.sub__location').find('.sub__depthbox').removeClass('actived');
      /*
      $('.animate_bg').addClass('scroll');
      setTimeout(function() {
        $('.animate_bg').hide();
      }, 1000);
      */

      // if($(this).scrollTop()){
      //   $('.wrap').addClass('sticky');
      // } else{
      //   $('.wrap').removeClass('sticky');
      // }
    });
  };
  return {
    init: init
  };
})();

// popupClose
var popupClose = (function () {
  var init = function () {
    var popupClose = $('[data-popup="close"]');
    $(popupClose).click(function(e){
      $(this).parent('.popup').hide();
    });
  };
  return {
    init: init
  };
})();

$('a[href="a#"],a[href="#a"]').click(function(e) {
  e.preventDefault();
});


//210601 | [QA]제품 팝업 내부에 닫기버튼 추가 210608 -> 직접추가
// let $closebtn = '<button class="product-info__closebtn" data-element="toggle__anchor" aria-label="정보창 닫기"></button>';
// $('.product-info__content').each(function(index, item){
//     $(item).append($closebtn)
// })
// // 210601 | [QA]제품 팝업 내부에 닫기버튼 추가 210608 -> 직접추가

//210602 | 제품 헤더 스크롤따라 열리고 닫힘
var headerPc = $('.b41Popup .popup__header');
var headerMob = $('.product-popup .popup__header');
if(headerPc.length) {
  var prevScrollTop = 0;
  var nowScrollTop = 0;
  $(".popup").on('mousewheel',function(e){
    var wheel = e.originalEvent.wheelDelta;
    
    //스크롤값을 가져온다.
    if(wheel>0 && headerPc.hasClass('actived')){
    //스크롤 올릴때
      console.log('내리는 중');
      headerPc.removeClass('actived')
    } else if(wheel<=0 && !headerPc.hasClass('actived')) {
    //스크롤 내릴때
        console.log('올리는 중');
        headerPc.addClass('actived')
    }
  });

}
if(headerMob.length) {
  var prevScrollTop = 0;
  var nowScrollTop = 0;
  function wheelDelta(){
      return prevScrollTop - nowScrollTop > 0 ? 'up' : 'down';
  };
  $('.popup__content').on('scroll', function(){
    nowScrollTop = $(this).scrollTop();
    if(wheelDelta() == 'down' && !headerMob.hasClass('actived')){
      headerMob.addClass('actived')
    }
    if((wheelDelta() == 'up' && headerMob.hasClass('actived')) || $(this).scrollTop() < 50){
      headerMob.removeClass('actived')
    }
    prevScrollTop = nowScrollTop;
  });
}
// //210602 | 제품 헤더 스크롤따라 열리고 닫힘