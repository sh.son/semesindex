function videoHeight(){
  $('.video, .main-swiper, .video__item, .video__background').height($(window).height())
}
$(document).ready(function () {
  // [s] main js
  videoHeight();
  HeaderCtrl.init(); // header
  if(navigator.userAgent.indexOf('Mobi') > -1){
    MobileHeaderCtrl.init(); // mobile header
    if($('.mouse-animation').length > 0){
      $('.sub').height($(window).height())
    }
  }
  SwiperCtrl.init(); // swiper
  InputFile.init();
  NewsGalleryCtrl.init();
  VideoCtrl.init();
  MapCtrl.init();
  Breadcrumbs.init();
  TopbuttonCtrl.init();
  // HorizontalInterection.init();
  scrollBg.init();
  popupClose.init();
  // scrollVar.init();
  // smoothScroll.init(); // smoothswheel
  // [e] main js

  // [s] parallax
  CommonVerticalParallax.init();
  CommonAnimationDirection.init();
  CommonHalfVerticalParallax.init();
  CommonScrollAnimation.init();
  MainParallax.init(); // main parallax
  MainMobileParallax.init(); // mobile main parallax
  // [e] parallax
  // smoothScroll.init(); // smoothswheel
  // if($('.sub__location').length && $('.clip').length){
  //   $('.sub__location').addClass('actived')
  // }
});
$(window).on('resize', function(){
  videoHeight();
})