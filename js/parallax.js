// !function(e,n){"function"==typeof define&&define.amd?define(["ScrollMagic","TweenMax","TimelineMax"],n):"object"==typeof exports?(require("gsap"),n(require("scrollmagic"),TweenMax,TimelineMax)):n(e.ScrollMagic||e.jQuery&&e.jQuery.ScrollMagic,e.TweenMax||e.TweenLite,e.TimelineMax||e.TimelineLite)}(this,function(e,n,o){"use strict";var t=window.console||{},r=Function.prototype.bind.call(t.error||t.log||function(){},t);e||r("(animation.gsap) -> ERROR: The ScrollMagic main module could not be found. Please make sure it's loaded before this plugin or use an asynchronous loader like requirejs."),n||r("(animation.gsap) -> ERROR: TweenLite or TweenMax could not be found. Please make sure GSAP is loaded before ScrollMagic or use an asynchronous loader like requirejs."),e.Scene.addOption("tweenChanges",!1,function(e){return!!e}),e.Scene.extend(function(){var e,t=this,r=function(){t._log&&(Array.prototype.splice.call(arguments,1,0,"(animation.gsap)","->"),t._log.apply(this,arguments))};t.on("progress.plugin_gsap",function(){i()}),t.on("destroy.plugin_gsap",function(e){t.removeTween(e.reset)});var i=function(){if(e){var n=t.progress(),o=t.state();e.repeat&&-1===e.repeat()?"DURING"===o&&e.paused()?e.play():"DURING"===o||e.paused()||e.pause():n!=e.progress()&&(0===t.duration()?n>0?e.play():e.reverse():t.tweenChanges()&&e.tweenTo?e.tweenTo(n*e.duration()):e.progress(n).pause())}};t.setTween=function(a,s,l){var c;arguments.length>1&&(arguments.length<3&&(l=s,s=1),a=n.to(a,s,l));try{(c=o?new o({smoothChildTiming:!0}).add(a):a).pause()}catch(e){return r(1,"ERROR calling method 'setTween()': Supplied argument is not a valid TweenObject"),t}if(e&&t.removeTween(),e=c,a.repeat&&-1===a.repeat()&&(e.repeat(-1),e.yoyo(a.yoyo())),t.tweenChanges()&&!e.tweenTo&&r(2,"WARNING: tweenChanges will only work if the TimelineMax object is available for ScrollMagic."),e&&t.controller()&&t.triggerElement()&&t.loglevel()>=2){var u=n.getTweensOf(t.triggerElement()),p=t.controller().info("vertical");u.forEach(function(e,n){var o=e.vars.css||e.vars;if(p?void 0!==o.top||void 0!==o.bottom:void 0!==o.left||void 0!==o.right)return r(2,"WARNING: Tweening the position of the trigger element affects the scene timing and should be avoided!"),!1})}if(parseFloat(TweenLite.version)>=1.14)for(var g,d,f=e.getChildren?e.getChildren(!0,!0,!1):[e],w=function(){r(2,"WARNING: tween was overwritten by another. To learn how to avoid this issue see here: https://github.com/janpaepke/ScrollMagic/wiki/WARNING:-tween-was-overwritten-by-another")},h=0;h<f.length;h++)g=f[h],d!==w&&(d=g.vars.onOverwrite,g.vars.onOverwrite=function(){d&&d.apply(this,arguments),w.apply(this,arguments)});return r(3,"added tween"),i(),t},t.removeTween=function(n){return e&&(n&&e.progress(0).pause(),e.kill(),e=void 0,r(3,"removed tween (reset: "+(n?"true":"false")+")")),t}})});

var CommonVerticalParallax = (function() {

  var init = function () {
    if ($('[data-role="parallax01"]').length < 1) {
      return;
    }
    action();
  }

  var action = function () {

    var controller = new ScrollMagic.Controller();

    var parallax = '.parallax01';
    var $height = $('.parallax01__bottom').height();
    setPinTop = new ScrollMagic.Scene({
      triggerElement: parallax,
      triggerHook: 0,
      offset: 0,
      duration: $height
    })
    .setPin(parallax + '__textarea', { pushFollowers : false })
    // .on('progress', function(e) {
    //   if(e.progress > 0.7) {
    //     $(parallax + '__inner').css({'visibility' : 'hidden'})
    //   } else {
    //     $(parallax + '__inner').css({'visibility' : 'visible'})
    //   }
    // })
    // .addIndicators({ name: 'setpin'})
    .addTo(controller);

    // function tabUpdate() {
    //   var tabs = $('[data-element="tab"]').data('plugin_tab');
    //   if(typeof tabs === 'undefined') {
    //     return;
    //   }
    //   $(tabs.element).on('afterChange', function () {
    //     var $height = document.querySelector('.parallax01__bottom').clientHeight;
    //     setPinTop.duration($height);
    //   });
    // }
    var triggerHookTime;
    if($(window).width() < 500){
      triggerHookTime = 0.7
    } else {
      triggerHookTime = 0.3
    }
    setPinBottom = new ScrollMagic.Scene({
      triggerElement: parallax + '__textarea',
      triggerHook: triggerHookTime,
      offset: 0,
      reverse: false
    })
    .on("start", function(){
      $.Velocity.RunSequence([
        { e: $(parallax + '__title'), p: { opacity: 1, bottom: 0}, o: {duration: 500}},
        { e: $(parallax + '__bar'), p: { opacity: 1, height: '120px'}, o: {duration: 500}}
      ]);
    })
    // .addIndicators({ name: 'bottom'})
    .addTo(controller)
  }
  return {
    init: init
  };
})();

var CommonHalfVerticalParallax = (function() {

  var init = function () {
    if ($('[data-role="parallax02"]').length < 1) {
      return;
    }
    // $(window).on({
    //   load: function() {
    //     updateParallax();
    //   },
    //   resize: function() {
    //     updateParallax();
    //   }
    // });
    action();
  }

  var updateParallax = function () {
    updateLeftDuration();
    setPinLeft.duration(updateLeftDurationCallback);
    updateRightDuration();
    setPinRight.duration(updateRightDurationCallback);
  }

  var updateLeftDurationCallback = function () {
    return leftDurationValue;
  }
  var updateRightDurationCallback = function () {
    return rightDurationValue;
  }

  var updateLeftDuration = function () {
    leftDurationValue = $('.parallax02__rightinner').outerHeight(true) - $(window).height();
  }
  var updateRightDuration = function () {
    rightDurationValue = $('.parallax03__leftinner').outerHeight(true) - $(window).height();
  }

  var action = function () {

    var controller = new ScrollMagic.Controller();

    var leftParallax = '.parallax02__left';
    setPinLeft = new ScrollMagic.Scene({
      triggerElement: leftParallax,
      triggerHook: 0,
      duration:500
    })
    .setPin(leftParallax)
    //.addIndicators({ name: 'setpin'})
    .addTo(controller);


    var rightParallax = '.parallax03__right';
    setPinRight = new ScrollMagic.Scene({
      triggerElement: rightParallax,
      triggerHook: 0,
      duration: 500
    })
    .setPin(rightParallax, {pushFollowers: true})
    // .addIndicators({ name: 'setpin'})
    .addTo(controller);

  }

  return {
    init: init
  };
})();

var CommonAnimationDirection = (function() {

  var init = function () {
    if ($('[data-animation="active"]').length < 1) {
      return;
    }
    action();
  }

  var action = function () {

    var controller = new ScrollMagic.Controller();
    var active = 'active'
    var fixed = 'fixed'
    var actived = 'actived'

    var img_scale = $('[data-animation="img_scale"]');
    var img_scale_end = $('[data-animation="img_scale"]').height() / 2;
    for(var i = 0; i < img_scale.length; i++) {
      scale06 = new ScrollMagic.Scene({
        triggerElement: img_scale[i],
        triggerHook: 0.5,
        offset: 0,
        duration: img_scale_end
      })
      .on('progress', function(e) {
      //  let gage = 1 + (e.progress * 0.1)
    //    let gage = 1 + (e.progress * 0.1 / 1)
        let gage = 1.1 - (e.progress / 10)
        $('[data-animation="img_scale"]').css({'transform' : 'scale(' + gage  +')'})
      })
      // .addIndicators({ name: 'test' })
      .addTo(controller)
    }

   
    var left = $('[data-animation="left"]')
    for(var i = 0; i < left.length; i++) {
      new ScrollMagic.Scene({
        triggerElement: left[i],
        offset: 0,
        triggerHook: 0.7,
        reverse: false
      })
      .on('start', function(e) {
        left.each(function(){
          var delay = $(this).data('animation-delay') / 1000;
          $(this).css({transition : 'all ' + delay + 's' + ' ease-out'})
        })
      })
      .setClassToggle(left[i], active)
      // .addIndicators({ name: 'left' })
      .addTo(controller);
    }

    var right = $('[data-animation="right"]')
    for(var i = 0; i < right.length; i++) {
      new ScrollMagic.Scene({
        triggerElement: right[i],
        offset: 0,
        triggerHook: 0.7,
        reverse: false
      })
      .on('start', function(e) {
        right.each(function(){
          var delay = $(this).data('animation-delay') / 1000;
          $(this).css({transition : 'all ' + delay + 's' + ' ease-out'})
        })
      })
      .setClassToggle(right[i], active)
      // .addIndicators({ name: 'right' })
      .addTo(controller);
    }

    var bottom = $('[data-animation="bottom"]')
    for(var i = 0; i < bottom.length; i++) {
      var hook;
      if(bottom.eq(i).hasClass('product-info__anchor')){
        hook = 0.9
      } else {
        hook = 0.7
      }
      new ScrollMagic.Scene({
        triggerElement: bottom[i],
        offset: -150,
        triggerHook: hook,
        reverse: false
      })
      .on('start', function(e) {
        bottom.each(function(){
          var delay = $(this).data('animation-delay') / 1000;
          $(this).css({transition : 'all ' + delay + 's' + ' ease-out'})
        })
      })
      .setClassToggle(bottom[i], active)
      //.addIndicators({ name: 'bottom' })
      .addTo(controller);
    }

    var advercard = $('[data-animation="advercard"]')
    for(var i = 0; i < advercard.length; i++) {
      new ScrollMagic.Scene({
        triggerElement: '#advercardTrigger',
        offset: 0,
        triggerHook: 0,
        reverse: true
      })
      .on('start', function(e) {
        advercard.each(function(){
          var delay = $(this).data('animation-delay') / 1000;
          $(this).css({transition : 'all ' + delay + 's' + ' ease-out'})
        })
      })
      .setClassToggle(advercard[i], active)
      // .addIndicators({ name: 'top' })
      .addTo(controller);
    }
    var adverstiky = $('[data-animation="adverstiky"]')
    for(var i = 0; i < adverstiky.length; i++) {
      new ScrollMagic.Scene({
        triggerElement: '#advercardTrigger',
        offset: 0,
        triggerHook: 0,
        reverse: true
      })
      .on('start', function(e) {
        adverstiky.each(function(){
          var delay = $(this).data('animation-delay') / 1000;
          $(this).find('.banner__inner--border').css({transition : 'all ' + delay + 's' + ' ease-out'})
          $(this).find('.title__small').css({transition : 'all ' + delay + 's' + ' ease-out'})
        })
      })
      .setClassToggle(adverstiky[i], active)
      //.addIndicators({ name: 'adadad' })
      .addTo(controller);
    }
    var adverstikybox = $('[data-animation="adverstikybox"]')
    for(var i = 0; i < adverstikybox.length; i++) {
      new ScrollMagic.Scene({
        triggerElement: '#boxTrigger',
        offset: 0,
        triggerHook: 0,
        reverse: true
      })
      // .on('start', function(e) {
      //   adverstikybox.each(function(){
      //     var delay = $(this).data('animation-delay') / 1000;
      //     $(this).find('.banner__inner--border').css({transition : 'all ' + delay + 's' + ' ease-out'})
      //     $(this).find('.title__small').css({transition : 'all ' + delay + 's' + ' ease-out'})
      //   })
      // })
      .setClassToggle(adverstikybox[i], fixed)
      //.addIndicators({ name: 'adadxxxxxxxad' })
      .addTo(controller);
    }
    var clipAction = $('.clip')
    var locationEl = $('.sub__location')
    for(var i = 0; i < clipAction.length; i++) {
      var thisEl = clipAction[i];
      var locEl = locationEl[i];
      new ScrollMagic.Scene({
        triggerElement: '.header',
        offset: 100,
        triggerHook: 0,
        reverse: true
      })
      .setClassToggle(clipAction[i], active)
      // .setClassToggle({thisEl: active, locEl: actived})
      // .setClassToggle(locationEl[i], actived)
      // .addIndicators({ name: 'adadxxxxxxxad' })
      .addTo(controller);
    }
  }
  return {
    init: init
  };
})();

$.fn.hasScrollBar = function() {
  return (this.prop("scrollHeight") == 0 && this.prop("clientHeight") == 0) || (this.prop("scrollHeight") > this.prop("clientHeight"));
};

var CommonScrollAnimation = (function() {

  var init = function () {
    if ($('[data-scroll="active"]').length < 1) {
      return;
    }
    event();
  }

  var event = function () {
    var flag = false;
    var windowHeight = 0;
    var lastOffset =	0;

    $(window).on({
      resize: function() {
        windowHeight = $(window).height();
      },
      scroll: function(e) {
        var currentOffset = $(this).scrollTop();
        if(flag){
          e.preventDefault();
          return;
        }
        if($(this).is(':animated')) {
          return;
        }
        if(currentOffset >= windowHeight) {
          return;
        }
        if(currentOffset > lastOffset) {
          if(currentOffset < windowHeight) {
            flag = true;
            $('html, body').velocity("scroll", {
              offset: windowHeight,
              // easing: 'easeOutQuart',
              // mobileHA: false,
              // duration: 600,
              complete: function () {
                setTimeout(function() {
                  lastOffset = $(window).scrollTop();
                  flag = false;
                }, 601);
              }
            })
          }
        } else {
          if(currentOffset <= windowHeight) {
            flag = true;
            $('html, body').velocity("scroll", {
              offset: 0,
              // easing: 'easeOutQuart',
              // mobileHA: false,
              // duration: 600,
              complete: function () {
                setTimeout(function() {
                  lastOffset = $(window).scrollTop();
                  flag = false;
                }, 601);
              }
            })
          }
        }
        lastOffset = currentOffset;
      }
    })
    $(window).trigger('resize');
  }

  return {
    init: init
  };
})();

var MainParallax = (function () {

  var init = function () {
    if ($('[data-role="mainparallax"]').length < 1) {
      return;
    }
    if($('[data-role="mainparallax"]').length >= 1 && $('[data-role="mainparallax"]').hasClass('c31')){
      actionC31();
    } else {
      action();
    }
  }
  
  var action = function () {
    var controller = new ScrollMagic.Controller();

    var bottomHeight = $('.mainparallax__bottom').height();
    parallax01 = new ScrollMagic.Scene({
      triggerElement: ".mainparallax",
      triggerHook: 0,
      offset: 0,
      duration: bottomHeight
    })
    .setPin(".mainparallax__section", { pushFollowers : false })
    .on('progress', function(e) {
      if(e.progress > 0.7) {
        $('.mainparallax__section .text-line').css({'visibility' : 'hidden'})
      } else {
        $('.mainparallax__section .text-line').css({'visibility' : 'visible'})
      }
    })
    // .addIndicators()
    .addTo(controller);

    animation01 = new ScrollMagic.Scene({
      triggerElement: '.mainparallax__section',
      triggerHook: 0.3,
      offset: 0,
      reverse: false
    })
    .on("start", function(){
      $.Velocity.RunSequence([
        { e: $('.mainparallax__section .fade-text'), p: { opacity: 1, bottom: 0}, o: {duration: 500}},
        { e: $('.mainparallax__section .fade-bar'), p: { height: '120px'}, o: {duration: 700}},
        { e: $('.mainparallax__section .fade-text2'), p: { opacity: 1, bottom: 0}, o: {duration: 1200}}
      ]);
    })
    // .addIndicators({ name: 'fadetext01' })
    .addTo(controller)

    var bottomHeight2 = $('.mainparallax02__bottom').height();
    parallax02 = new ScrollMagic.Scene({
      triggerElement: ".mainparallax02",
      triggerHook: 0,
      offset: 0,
      duration: bottomHeight2
    })
    .setPin(".mainparallax02__section", { pushFollowers : false })
    .on('progress', function(e) {
      if(e.progress > 0.7) {
    //    $('.mainparallax02__section .text-line').css({'visibility' : 'hidden'})
      } else {
    //    $('.mainparallax02__section .text-line').css({'visibility' : 'visible'})
      }
    })
    // .addIndicators()
    .addTo(controller);

    animation02 = new ScrollMagic.Scene({
      triggerElement: '.mainparallax02__section',
      triggerHook: 0.3,
      offset: 0,
      reverse: false
    })
    .on("start", function(){
      $.Velocity.RunSequence([
        { e: $('.mainparallax02__section .fade-text'), p: { opacity: 1, bottom: 0}, o: {duration: 500}},
        { e: $('.mainparallax02__section .fade-bar'), p: { height: '120px'}, o: {duration: 500}},
        { e: $('.mainparallax02__section .fade-text2'), p: { opacity: 1, bottom: 0}, o: {duration: 1200}}
      ]);
    })
    // .addIndicators({ name: 'fadetext02' })
    .addTo(controller)


    var scaleHeight = $('.z00s03').height();
    scale01 = new ScrollMagic.Scene({
      triggerElement: '.z00s03',
      triggerHook: 0.5,
      offset: 0,
      duration: scaleHeight
    })
    .on('progress', function(e) {
      let gage = 1.1 - (e.progress / 10)
      $('.company-info__bg--type01').css({'transform' : 'scale(' + gage  +')'})
    })
    // .addIndicators({ name: 'test' })
    .addTo(controller)

    var scaleHeight2 = $('.z00s04').height();
    scale02 = new ScrollMagic.Scene({
      triggerElement: '.z00s04',
      triggerHook: 0.5,
      offset: 0,
      duration: scaleHeight2
    })
    .on('progress', function(e) {
      let gage = 1.1 - (e.progress / 10)
      $('.company-info__bg--type02').css({'transform' : 'scale(' + gage  +')'})
    })
    // .addIndicators()
    .addTo(controller)

    var scaleHeight3 = $('.z00s05').height();
    scale03 = new ScrollMagic.Scene({
      triggerElement: '.z00s05',
      triggerHook: 0.5,
      offset: 0,
      duration: scaleHeight3
    })
    .on('progress', function(e) {
      let gage = 1.1 - (e.progress / 10)
      $('.company-info__bg--type03').css({'transform' : 'scale(' + gage  +')'})
    })
    // .addIndicators()
    .addTo(controller)
  }
  var actionC31 = function(){
    var controller = new ScrollMagic.Controller();
    animation01 = new ScrollMagic.Scene({
      triggerElement: '.mainparallax__section',
      triggerHook: 0.3,
      offset: 0,
      reverse: false
    })
    .on("start", function(){
      $.Velocity.RunSequence([
        { e: $('.mainparallax__section .fade-text'), p: { opacity: 1, bottom: 0}, o: {duration: 500}},
        { e: $('.mainparallax__section .fade-bar'), p: { height: '120px'}, o: {duration: 700}},
        { e: $('.mainparallax__section .fade-text2'), p: { opacity: 1, bottom: 0}, o: {duration: 1200}}
      ]);
    })
    // .addIndicators({ name: 'fadetext01' })
    .addTo(controller)
  }
  return {
    init: init,
  };
})();

var MainMobileParallax = (function () {

  var init = function () {
    if ($('[data-role="mobileparallax"]').length < 1) {
      return;
    }
    action();
  }
  var action = function () {

    var controller = new ScrollMagic.Controller();

    var bottomHeight = $('.mainparallax__bottom').height();
    section01 = new ScrollMagic.Scene({
      triggerElement: ".mainparallax",
      triggerHook: 0,
      offset: 0,
      duration: bottomHeight
    })
      .setPin(".mainparallax__section", { pushFollowers: false })
      // .addIndicators({ name: 'test' })
      .addTo(controller);

    var bottomHeight2 = $('.mainparallax02__bottom').height();
    section02 = new ScrollMagic.Scene({
      triggerElement: ".mainparallax02",
      triggerHook: 0,
      offset: 0,
      duration: bottomHeight2
    })
      .setPin(".mainparallax02__section", { pushFollowers: false })
      // .addIndicators({ name: 'text01' })
      .addTo(controller);

    var mainTriggerHookTime;
    if($(window).width() < 500){
      mainTriggerHookTime = 0.7
    } else {
      mainTriggerHookTime = 0.3
    }
    animation01 = new ScrollMagic.Scene({
      triggerElement: '.z00s02',
      triggerHook: mainTriggerHookTime,
      offset: 0,
      reverse: false
    })
    .on("start", function(){
      $.Velocity.RunSequence([
        { e: $('.mainparallax__section .text-line__inner .text-line__title'), p: { opacity: 1, bottom: 0}, o: {duration: 500}},
        { e: $('.mainparallax__section .text-line__vertical-line'), p: { height: '80px'}, o: {duration: 600}},
        { e: $('.mainparallax__section .text-line__inner .text-line__content'), p: { opacity: 1, bottom: 0}, o: {duration: 700}},
      ]);
    })
    // .addIndicators()
    .addTo(controller)

    var mainTriggerHook2Time;
    if($(window).width() < 500){
      mainTriggerHook2Time = 0.7
    } else {
      mainTriggerHook2Time = 0.3
    }
    animation02 = new ScrollMagic.Scene({
      triggerElement: '.z00s06',
      triggerHook: mainTriggerHook2Time,
      offset: 0,
      reverse: false
    })
    .on("start", function(){
      $.Velocity.RunSequence([
        { e: $('.mainparallax02__section .text-line__inner .text-line__title'), p: { opacity: 1, bottom: 0}, o: {duration: 500}},
        { e: $('.mainparallax02__section .text-line__vertical-line'), p: { height: '80px'}, o: {duration: 600}},
        { e: $('.mainparallax02__section .text-line__inner .text-line__content'), p: { opacity: 1, bottom: 0}, o: {duration: 700}}
      ]);
    })
    // .addIndicators()
    .addTo(controller)
  }
  return {
    init: init
  };
})();
